# Disk Replacement 

## Procedure for Replacing a disk.

To place a new Disk in `offline` the Disk you want to take out first. Place the new Disk in and then use the `replace` command to bring it into the Pool. If the disk is already out `detach` the disk from the pool and then you can also use `attach` instead of `replace`. Both `attach` and `replace` commands work with mirror configuration, for raidz only `replace` can be used. Choose from one of the options below for replacing a Disk.


Before the Disk can be added to the Pools it needs to be partitioned. 

Following this [OpeZFS guide](https://openzfs.github.io/openzfs-docs/Getting%20Started/Ubuntu/Ubuntu%2020.04%20Root%20on%20ZFS.html#step-2-disk-formatting), only certain steps are needed when adding a new Disk. 


## Offline the bad Disk

```sh
zpool offline bpool scsi-35000c500170f6fb7-part3
```

```sh
zpool offline rpool scsi-35000c500170f6fb7-part4
```

OR

```sh
zpool detach bpool scsi-35000c500170f6fb7-part3
```

```sh
zpool detach rpool scsi-35000c500170f6fb7-part4
```


If `/boot/efi` is mounted from this Disk unmount it.

```sh
sudo umount /boot/efi
```

## Select the new Disk

Most likely the new Disk will be new and will not have any partitions.

To list all the disks:

```sh
ls -al /dev/disk/by-id
```
![disks](images/phoenicdisks.png)

Compare the new Disk id with old ones. 

Select the new disk:

```sh
DISK=/dev/disk/by-id/scsi-35000c500170f6fb7
```


## mdadm

Check if the Disk was previously used in an MD array:

```sh
# See if one or more MD arrays are active:
cat /proc/mdstat
# If so, stop them (replace ``md0`` as required):
mdadm --stop /dev/md0

# For an array using the whole disk:
mdadm --zero-superblock --force $DISK
# For an array using a partition (e.g. a swap partition per this HOWTO):
mdadm --zero-superblock --force ${DISK}-part2
```

## Clear the partition table

Make sure you dont't select the wrong Disk.

```sh
sgdisk --zap-all $DISK
```

## Partition the Disk into 3 partitions


Create bootloader partition(s):
```sh
sgdisk     -n1:1M:+512M   -t1:EF00 $DISK
```

Create a boot pool partition:
```sh
sgdisk     -n3:0:+2G      -t3:BE00 $DISK
```

Create a root pool partition:
```sh
sgdisk     -n4:0:0        -t4:BF00 $DISK
```



## Replace Disk (Option 1)

Replace `rpool` partition, enter old Disk followed by new disk id.
```sh
zpool replace rpool scsi-35000c500170f6fb7-part4 scsi-35000c500170f6fb7-part4
```

Replace `bpool` partition, enter old Disk followed by new disk id.
```sh
zpool replace bpool scsi-35000c500170f6fb7-part3 scsi-35000c500170f6fb7-part3
```
## Detach and Attach Disk (Option 2)

Detach `bpool` partition, enter old Disk id.
```sh
zpool detach bpool scsi-35000c500170f6fb7-part4
```
Detach `rpool` partition, enter old Disk id.
```sh
zpool detach rpool scsi-35000c500170f6fb7-part4
```

Attach `rpool` partition, enter one of the working Disk id's followed by the new Disk id.
```sh
zpool attach rpool scsi-35000c500170f6fb7-part3 scsi-35000c500170f6fb7-part3
```
Attach `bpool` partition, enter one of the working Disk id's followed by the new Disk id.
```sh
zpool attach bpool scsi-35000c500170f6fb7-part3 scsi-35000c500170f6fb7-part3
```

## /boot/efi

Format partition 1.
```sh
mkdosfs -F 32 -s 1 -n EFI ${DISK}-part1
```
Echo the disk id to fstab. The command will append a line as a comment.
```sh
echo \#/dev/disk/by-uuid/$(blkid -s UUID -o value ${DISK}-part1) \
    /boot/efi vfat defaults 0 0 >> /etc/fstab
```

If the Disk that was taken out was used in `fstab` to mount `/boot/efi` then `/etc/fstab` needs to be edited and a new Disk uncommented so `/boot/efi` can be mounted from it at boot, and old Disk id removed. 

Old Disk id needs to be removed anyway after the Disk is taken out as it is not needed anymore. 

If needed mount the /boot/efi
```sh
mount /boot/efi
```
Check if it's mounted 
```sh
lsblk
```


![lsblk](images/phoenicpartitions.png)


## Install GRUB to additional disks

Select the newly added Disk (using the space bar) along with all of the other 3 ESP partitions (partition 1 on
each three other pool disks).

```sh
dpkg-reconfigure grub-efi-amd64
```

## Reboot

It is not necessary to reboot but if rebooting make sure `/etc/fstab` has an uncommented entry for  `/boot/efi` to mount from at boot time.

Even without proper `fstab` in a mirror configuration Ubuntu will still boot with delay. 


In raidz configuration if `/boot/efi` can not be mounted, maintenance mode will display. Edit the `fstab` and exit. In this case a KVM needs to be used. 

![maintenance](images/maintenance.jpg)


## YouTube Video

[Adding a Disk using these instructions](https://youtu.be/c_3odGb9ihA)

[Adding a Disk using OpenZFS guide](https://youtu.be/AnDN3HC7wcE)

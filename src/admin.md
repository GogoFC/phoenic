# ZFS Administration


## Create a Dataset

```sh
zfs create rpool/USERDATA/sky3853_2bv9ey/newdataset
```

## Destroy a Dataset

```sh
zfs destroy rpool/USERDATA/sky3853_2bv9ey/snapnomount
```
Use `-r` flag to recursively delete any Child Datasets or snapshots of the Dataset.

```sh
zfs destroy -r rpool/USERDATA/sky3853_2bv9ey/snapnomount
```

## List Datasets

List datasets and mountpoints.

```sh
zfs list
```

Using the `-r` flag to recursively list all Datasets on `rpool/USERDATA`.

```sh
zfs list -r rpool/USERDATA
```

## Show mounted

Shows only mounted Datasets and mountpoints. 

```sh
zfs mount
```

## Mount a Dataset

```sh
zfs unmount rpool/USERDATA/sky3853_kiof56
```
Mount all Datasets.

```sh
zfs mount -a
```

## Unmount a Dataset

```sh
zfs unmount rpool/USERDATA/sky3853_kiof56
```


## ZFS Delegated Permissions

Give user sky3853 permissions on a dataset.

```sh
zfs allow sky3853 compression,create,mount,mountpoint,receive,rollback,send,snapshot,destroy rpool/USERDATA/sky3853_2bv9ey
```
Unallow user sky3853 permissios on a dataset.

```sh
zfs unallow sky3853 compression,create,mount,mountpoint,receive,rollback,send,snapshot,destroy rpool/USERDATA/sky3853_2bv9ey
```


## Taking Snapshots


To take a snapshot of dataset `rpool/USERDATA/sky3853_2bv9ey` enter a name after `@`.

```sh
zfs snapshot rpool/USERDATA/sky3853_2bv9ey@SnapshotName
```

## List Snapshots

List all Snapshots including received unmounted ones.

```sh
zfs list -t snapshot
```

## Destroying Snapshots


Destroy a snapshot.

```sh
zfs destroy rpool/USERDATA/sky3853_2bv9ey@t353
```

Delete all snapshots on dataset `sky3853_2bv9ey`, by adding `%` after `@`.

```sh
zfs destroy rpool/USERDATA/sky3853_2bv9ey@%
```

## Rolling back Snapshots

Rolling back to a Snapshot will revert the Dataset back to what it was at that time. Data currently on it will be replaced by data from the Snapshot, so if you need the current data move it elsewhere. All consequent Snapshots will be destroyed, ZFS will complain you have to use `-r` flag.

```sh
zfs rollback -r rpool/USERDATA/sky3853_2bv9ey@t
```

## Send and Receive Snapshots

Send and Receive a Snapshot to a new Dataset which also gets mounted automatically as a new Filesystem.

```sh
zfs send rpool/USERDATA/sky3853_2bv9ey@today | zfs recv rpool/USERDATA/newdataset
```

## Nomount flag

When sending and receiving Snapshot use `-u` flag to tell ZFS not to try to mount it.

```sh
zfs send rpool/USERDATA/sky3853_2bv9ey@today | zfs recv -u rpool/USERDATA/sky3853_2bv9ey/newdataset
```

## Send over SSH

Sudo is needed to mount the Received Snapshot. 


```sh
zfs send rpool/USERDATA/sky3853_2bv9ey@today | ssh sky3853@10.0.0.10 sudo zfs recv rpool/USERDATA/SomeNewDataset
```
A new Snapshot at remote server of the newly transferred Dataset will get created with the same name as the new Dataset. Snaphot name can be changed by adding `@` after Dataset name like so `SomeNewDataset@newsnapname`. 

## Incremental Snapshot

When sending incremental Snapshots use the flag `-i`, and use the flag `-F` for receiving. 

Flag `-F` forces rollback or Dataset, as the data has now changed. 

```sh
zfs send -i rpool/USERDATA/user0_sx0jq9@inc rpool/USERDATA/user0_sx0jq9@inc4 | ssh sky3853@localhost zfs recv -F rpool/USERDATA/sky3853_2bv9ey/snapnomount
```

## Reservation Dataset

`rpool/ROOT/reservation`

```sh
NAME                    PROPERTY     VALUE   SOURCE
rpool/ROOT/reservation  reservation  1.80T   local
```


Set a Reservation of 1.8TB on Dataset.

```sh
sudo zfs set reservation=1.8T rpool/ROOT/reservation
```
Get Reservation info on Dataset.

```sh
zfs get reservation rpool/ROOT/reservation
```

Destroy the Datatset `reservation`.

```sh
sudo zfs destroy rpool/ROOT/reservation
```


## Documentation

[illumos ZFS Administration Guide ](https://illumos.org/books/zfs-admin/snapshots.html)


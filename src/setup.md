# Server Setup

## Installation

Server is setup according to [OpenZFS root on ZFS Instructions.](https://openzfs.github.io/openzfs-docs/Getting%20Started/Ubuntu/Ubuntu%2020.04%20Root%20on%20ZFS.html#)

## Partitions

There are 3 partitions on each Disk.

- EFI Boot partition.
- bpool partition.
- rpool partition.

![partitions](images/phoenicpartitions.png)

## Zpool


There are two ZFS Pools `bpool` and `rpool` on 4 Disks in a Mirror configuration. Pools are not encrypted.

![partitions](images/phoeniczpool.png)

## Users

User datasets are created on `rpool/USERDATA` and mounted as their home directory.

## Network

TODO: Netplan, bond

Config file `/etc/netplan/`.



## EFI filesystem

At boot time `/boot/efi` is mounted from one Disk using fstab entry. 

When a Disk fails and a new one is added `/etc/fstab` file needs to be edited to add the new Disk and old entry removed.

If the disk that is removed was used to mount `/boot/efi` from then remove the line and uncomment another disk.

![fstab](images/phoenicfstab.png)

## zsys

System-wide auto-snapshots are turned on, this can be turned off.

By default auto-snapshots for users are on.

Users can turn on or off auto-snapshots on their own dataset. 

## Encryption Keys

Keys for encrypted user Datasets are kept at `/.locks`

These are never needed, they are used at boot time to decrypt the Datasets.

They could be used if a Dataset needed to be unmounted and Key unloaded so that the Dataeset can't be read or accessed. 

## Crontab for root

Set to:
- scrub `bpool` daily, and `rpool`  monthly.

- Runs `zstatus.sh` script daily which sends an email if a pool is degraded.

```sh
0 1 * * *  /sbin/zpool scrub bpool
0 2 1 * * /sbin/zpool scrub rpool
5 * * * * /root/______*_____/zstatus.sh
```

## Root scripts

Located in `/root` directory.

- `zstatus.sh` - Checks for pool degradation and sends an email. 

## Administrator scipts 

Located in Administrator's `/home/` directory.

- `adduserencrypted.sh` - Adds a new user and encrypts the dataset on their home directory.

- `adduser.sh` - Adds a new user doesn't encrypt their home directory.

## User scripts

Users receive the followng scripts in their home directories after they are created via adduser scripts. 

- `info.sh` - Displays relevant info regarding space available. 

- `DisableAutoSnapshots.sh` - Disables Auto Snapshots. 

- `EnableAutoSnapshots.sh` - Enables Auto Snapshots. 

## Administrator directories

Located in Administrator's `/home/` directory.

- `scripts` directory contains scripts for creating users. 

- `forusers` directory contains scripts that are copied to users when created.

- `user_home_dir_backup` directory contains copies of users' `.profile` and the scripts they received. 

## Notifications

ooo send notifications in `zstatus.sh` script.



## Reservation Dataset

%%%%%% NOT Implemented yet

`rpool/ROOT/reservation`

```sh
NAME                    PROPERTY     VALUE   SOURCE
rpool/ROOT/reservation  reservation  100G   local
```

A Dataset named `reservation` is created, and a ZFS Reservation on it is made to made sure all of the space in the Pool doesn't get used. The Reservation can be set to a lower number when needed or set to none. Dataset can also be destroyed. Reservation also prevents the Pool not get 100% full which would slow the Pool down as fragmentation happens when the Pool is almost full. 
